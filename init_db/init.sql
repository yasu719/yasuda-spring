CREATE TABLE users (
  id SERIAL,
  name VARCHAR(255) NOT NULL, /** ユーザー名 */
  status VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE chat (
  name  VARCHAR,
  message VARCHAR,
  updated_time timestamp
);
INSERT INTO users (name, status) values ('test', 'テスト');
INSERT INTO users (name, status) values ('wada-puyo', 'ぷよぷよ');
INSERT INTO users (name, status) values ('wada-puni', 'ぷにぷに');

CREATE ROLE testuser WITH LOGIN PASSWORD 'testpass';
GRANT SELECT,UPDATE,INSERT,DELETE ON ALL TABLES IN SCHEMA public TO testuser;