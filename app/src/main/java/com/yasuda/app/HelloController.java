package com.yasuda.app.thymeleaf.web;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller

public class HelloController {

    @Autowired
	JdbcTemplate jdbcTemplate;


    @GetMapping("/hello")
    public String hello(Model model) {
        model.addAttribute("message", "Hello Thymeleaf!!");
        return "hello";
    }

    @GetMapping("/test")
    public String hello2(Model model) {
        // List<Map<String,Object>> list;
        // list = jdbcTemplate.queryForList("select * from chat");
        // model.addAttribute("message", "Hello2 Thymeleaf!!");
        // model.addAttribute("list", list);
        // model.addAttribute("message", "Hello2 Thymeleaf!!");
        return "message_board";
    }

    @RequestMapping("/dbchat")
    public String dbchat(Model model) {
        List<Map<String,Object>> list;
        list = jdbcTemplate.queryForList("select * from chat");
        return list.toString();
    }

}